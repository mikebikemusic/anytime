#include <pebble.h>
#include <pebble_fonts.h>

extern void init(void);
extern void deinit(void);

int main(void) {
  init();
  app_event_loop();
  deinit();
}
