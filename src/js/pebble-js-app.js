var initialized = false;

     function getLocation(latitude, longitude){
        var request = new XMLHttpRequest();

        var method = 'GET';
        var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true';
        var async = true;

        request.open(method, url, async);
        request.onreadystatechange = function(){
          if(request.readyState == 4 && request.status == 200){
            var data = JSON.parse(request.responseText);
            var address = data.results[0];
            var city = { "city" : address.address_components[3].short_name }
			console.log("city = " + JSON.stringify(city));
			//Pebble.sendAppMessage(encodeURIComponent(JSON.stringify(city)));
			Pebble.sendAppMessage(city);
          }
        };
        request.send();
      }

function locationSuccess(pos) {
	Pebble.sendAppMessage({ "latitude": pos.coords.latitude * 1000000, "longitude": pos.coords.longitude * 1000000 });
    getLocation(pos.coords.latitude, pos.coords.longitude);
}

function locationError(err) {
}

var locationOptions = { enableHighAccuracy: true, "timeout": 15000, "maximumAge": 60000 }; 

Pebble.addEventListener("ready", function() {
	console.log("ready called!");
	var d = new Date();
	var n = d.getTimezoneOffset();
	Pebble.sendAppMessage({ "TZOffset": n});
	locationWatcher = window.navigator.geolocation.watchPosition(locationSuccess, locationError, locationOptions);
	initialized = true;
});

Pebble.addEventListener("showConfiguration", function() {
	console.log("showing configuration");
	Pebble.openURL('http://glenvenue.com/pebble/tz_config3.html');
});

Pebble.addEventListener("webviewclosed", function(e) {
	console.log("configuration closed");
	// webview closed
	var options = JSON.parse(decodeURIComponent(e.response));
	console.log("Options = " + JSON.stringify(options));
	Pebble.sendAppMessage(options);
});
