#include "pebble.h"

enum {
	AKEY_TZOFFSET,
	AKEY_TIMEZONE,
	AKEY_INVERTED,
	AKEY_TIMESTYLE,
	AKEY_LONGITUDE,
	AKEY_TZTYPE,
	AKEY_STYLE,
	AKEY_LOCALOFFSET,
	AKEY_LATITUDE,
	AKEY_CITY,
	AKEY_TZCITY,
	AKEY_TZSECONDS
};

enum  tzTypes {
	tzUTC,
	tzLocal,
	tzSidereal,
	tzSelected
};

typedef struct {
	char style[2][20];
	int style_font[2];
	int tzType;
	time_t selected;
	time_t tzoffset;
	struct tm now;
	Layer *layer;
} timezone_t;

#define FONT_COUNT 6
static Window *window;
static GFont font[FONT_COUNT];
//**************************************
static int UTCOffset = 0;
static time_t localSiderealOffset;
static float our_longitude;
static int our_longitude_int;
static char our_style[40];
static char timeFmt[40];
static char timeStyle[40];
static char myCity[80];
static char tzCity[80];
static bool usePeriod = false;
static int time_font;

#define LOG  

#define NUM_TIMEZONES 2
#define PEBBLE_SCREEN_HEIGHT 168
#define PEBBLE_SCREEN_WIDTH 144
#define LAYER_HEIGHT (PEBBLE_SCREEN_HEIGHT / NUM_TIMEZONES)
#define FIFTEEN_MINUTES 900

#define SECONDS_PER_DAY 86400
#define solarDayDegrees 360.985647366
	
#define siderealDegreesNovember15_2013 54.2381662923987
#define unixtimeNovember15_2013 1384473600


static timezone_t timezones[NUM_TIMEZONES] =
{
	{ .selected = 0,.tzoffset=0, .tzType = tzLocal },
	{ .selected = 0, .tzoffset=0, .tzType = tzUTC },
};
 
#define TZNAMELAST 104
static const char *tzNames[] =
{
	"UTC-12 BIT", "", "", "",
	"UTC-11 NUT", "", "", "",
	"UTC-10 HST", "", "UTC-9:30 MART", "",
	"UTC-9 AKST", "", "", "",
	"UTC-8 PST", "", "", "",
	"UTC-7 MST PDT", "", "", "",
	"UTC-6 CST MDT", "", "", "",
	"UTC-5 EST CDT", "", "UTC-4:30 VET", "",
	"UTC-4 AMT EDT", "", "UTC-3:30 NST", "",
	"UTC-3 ADT AMST", "", "", "",
	"UTC-2 UYST", "", "", "",
	"UTC-1 CVT", "", "", "",
	"UTC GMT ZULU", "", "", "",
	"UTC+1 BST CET", "", "", "",
	"UTC+2 SAST", "", "", "",
	"UTC+3 EAT", "", "UTC+3:30 IRST", "",
	"UTC+4 UAE", "", "UTC+4:30 AFT", "",
	"UTC+5 AMST", "", "UTC+5:30 IST", "UTC+5:45 NPT",
	"UTC+6 BTT", "", "UTC+6:30 MST", "",
	"UTC+7 ICT", "", "", "",
	"UTC+8 ACT", "", "", "UTC+8:45 CWST",
	"UTC+9 JST KST", "", "UTC+9:30 ACST", "",
	"UTC+10 AEST", "", "UTC+10:30 ACDT", "",
	"UTC+11 AEDT", "", "UTC+11:30 NFT", "",
	"UTC+12 NZST", "", "", "UTC+12:45",
	"UTC+13 NZDT", "", "", "UTC+13:45",
	"UTC+14 LINT", "", "", "",
};
static const char *tzNamesShort[] =
{
	"UTC-12", "", "", "",
	"UTC-11", "", "", "",
	"UTC-10", "", "UTC-9+", "",
	"UTC-9", "", "", "",
	"UTC-8", "", "", "",
	"UTC-7", "", "", "",
	"UTC-6", "", "", "",
	"UTC-5", "", "UTC-4+", "",
	"UTC-4", "", "UTC-3+", "",
	"UTC-3", "", "", "",
	"UTC-2", "", "", "",
	"UTC-1", "", "", "",
	"UTC", "", "", "",
	"UTC+1", "", "", "",
	"UTC+2", "", "", "",
	"UTC+3", "", "UTC+3+", "",
	"UTC+4", "", "UTC+4+", "",
	"UTC+5", "", "UTC+5+", "UTC+5^",
	"UTC+6", "", "UTC+6+", "",
	"UTC+7", "", "", "",
	"UTC+8", "", "", "UTC+8^",
	"UTC+9", "", "UTC+9+", "",
	"UTC+10", "", "UTC+10+", "",
	"UTC+11", "", "UTC+11+", "",
	"UTC+12", "", "", "UTC+12^",
	"UTC+13", "", "", "UTC+13^",
	"UTC+14", "", "", "",
};
static const char *tzNamesDiff[] =
{
	"-12", "", "", "",
	"-11", "", "", "",
	"-10", "", "-9+", "",
	"-9", "", "", "",
	"-8", "", "", "",
	"-7", "", "", "",
	"-6", "", "", "",
	"-5", "", "-4+", "",
	"-4", "", "-3+", "",
	"-3", "", "", "",
	"-2", "", "", "",
	"-1", "", "", "",
	"+0", "", "", "",
	"+1", "", "", "",
	"+2", "", "", "",
	"+3", "", "+3+", "",
	"+4", "", "+4+", "",
	"+5", "", "+5+", "+5^",
	"+6", "", "+6+", "",
	"+7", "", "", "",
	"+8", "", "", "+8^",
	"+9", "", "+9+", "",
	"+10", "", "+10+", "",
	"+11", "", "+11+", "",
	"+12", "", "", "+12^",
	"+13", "", "", "+13^",
	"+14", "", "", "",
};

#define INT_DIGITS 12		/* enough for 32 bit integer */

#if 0
static char *itoa(int i)
{
  /* Room for INT_DIGITS digits, - and '\0' */
  static char buf[INT_DIGITS + 2];
  char *p = buf + INT_DIGITS + 1;	/* points to terminating '\0' */
  if (i >= 0) {
    do {
      *--p = '0' + (i % 10);
      i /= 10;
    } while (i != 0);
    return p;
  }
  else {			/* i < 0 */
    do {
      *--p = '0' - (i % 10);
      i /= 10;
    } while (i != 0);
    *--p = '-';
  }
  return p;
}
#endif

#define FLOAT_DIGITS 20
char *floatToString(float f, char* floatbuf /*, int precision=10, char plus=' ', char minus='-', int modulo=0*/)
{
	int precision=8;
	char plus=' ';
	char minus='-'; 
	int modulo=0;
	char *p = floatbuf + FLOAT_DIGITS - 1;
	*--p = 0;
	float degrees = f;
	if (degrees < 0.0) {
		degrees = -f;
		*--p = minus;
	} else  if (plus != '+') {
		*--p = plus;
	}
		
	int whole = degrees;
	float wholef = whole;
	float fractionf = degrees - wholef;
	if (modulo != 0)
		whole = whole % modulo;
	
	for (int i = precision; i > 0; --i) {
		fractionf = fractionf * 10.0;
	}
	int fraction = fractionf;
	for (int i = precision; i > 0; --i) {
      	*--p = '0' + (fraction % 10);
    	fraction /= 10;
    };
    *--p = '.';
	
    while (whole != 0) {
    	*--p = '0' + (whole % 10);
    	whole /= 10;
    } 
	return p;
}


void calcSiderealOffset() {
	if (timezones[1].tzType != tzSidereal)
		return;
	time_t unixtimelocal = time(NULL);
	time_t unixtime = unixtimelocal + UTCOffset;
	float daysSince = unixtime - unixtimeNovember15_2013;
	daysSince /= SECONDS_PER_DAY;
	
	float GSTdegrees = (solarDayDegrees * daysSince) + siderealDegreesNovember15_2013;
	
	float LSTdegrees = GSTdegrees + our_longitude;
	time_t localSiderealTime = LSTdegrees * 240.0 + unixtimeNovember15_2013;
	localSiderealOffset = localSiderealTime - unixtimelocal;
	timezones[1].tzoffset = localSiderealOffset;
}

static int containerOf(Layer *layer)
{
	for (int i = 0 ; i < NUM_TIMEZONES ; i++) {
           if (layer == timezones[i].layer)
				return i;
	}
	return 0;
}

void calcStyles() // example: p$2|$g|pa pdpbpg|2|$G|pa|
{
	char *stylePtr = our_style;
	char pct = *stylePtr++;		// % messes up javascript
	char fmtChar = *stylePtr++;	// Suggested format char is $

	for (int i = 0 ; i < NUM_TIMEZONES ; i++) {
		int index = 48 - (timezones[i].selected / FIFTEEN_MINUTES);
		timezones[i].style[1][0] = 0;
		int count = *stylePtr++ - '0';
		char sep = *stylePtr++;		// Suggested separator char is |
		for (int j = 0; j < count; j++) {
			char *fmtPtr = timezones[i].style[j];
			timezones[i].style_font[j] = *stylePtr++ - 'A';
			if (timezones[i].style_font[j] < 0 || timezones[i].style_font[j] >= FONT_COUNT) {
				APP_LOG(APP_LOG_LEVEL_DEBUG,"bad font size: %s", stylePtr-1);
				timezones[i].style_font[j] = 0;
			}
			APP_LOG(APP_LOG_LEVEL_DEBUG,"style for %d,%d type %d: %s font %d", i, j, timezones[i].tzType, stylePtr, timezones[i].style_font[j]);
			while (*stylePtr != sep && *stylePtr != 0) {
				if (*stylePtr == fmtChar) {
					stylePtr++;
					char style = *stylePtr++;
					switch (style) {
						case 'G':
							strcpy(fmtPtr, tzNames[index]);
							fmtPtr += strlen(tzNames[index]);
							break;
						case 'g':
							strcpy(fmtPtr, tzNamesShort[index]);
							fmtPtr += strlen(tzNamesShort[index]);
							break;
						case 'D':
							strcpy(fmtPtr, tzNamesDiff[index]);
							fmtPtr += strlen(tzNamesDiff[index]);
							break;
						case 'a':
						case 'A':
						{
							time_t localUnixTime = time(NULL);
							time_t layerTime = localUnixTime + timezones[i].tzoffset;
							if (localUnixTime / SECONDS_PER_DAY != layerTime / SECONDS_PER_DAY) {
								*fmtPtr++ = '%';
								*fmtPtr++ = style;
							}
						}
							break;
						case 'C':
							if (timezones[i].tzType == tzSelected) {
								strcpy(fmtPtr, tzCity);
								fmtPtr += strlen(tzCity);
							} else {
								strcpy(fmtPtr, myCity);
								fmtPtr += strlen(myCity);
							}
							break;						
					}
				} else if (*stylePtr == pct) {
					*fmtPtr++ = '%';
					stylePtr++;
				} else
					*fmtPtr++ = *stylePtr++;
			}
			if (*stylePtr != 0)
				stylePtr++; // go past sep
			*fmtPtr = 0;
		}
	}
}

void calcTimeFmt()
{
	usePeriod = false;
	if (timezones[1].tzType == tzSidereal)
		timezones[1].tzType = tzSelected;
	char *stylePtr = timeStyle+1;
	char pct = *stylePtr++;		// % messes up javascript
	char *fmtPtr = timeFmt;
	time_font = *stylePtr++ - 'A';
	if (time_font < 0 || time_font >= FONT_COUNT) {
		APP_LOG(APP_LOG_LEVEL_DEBUG,"bad font size: %s", stylePtr-1);
		time_font = 0;
	}
	while (*stylePtr != 0) {
		if (*stylePtr == pct) {
			*fmtPtr++ = '%';
			stylePtr++;
		} else if (*stylePtr == '$') {
			stylePtr++;
			char style = *stylePtr++;
			switch (style) {
				case 'p':
					usePeriod = true;
					break;
				case 'S':
					timezones[1].tzType = tzSidereal;
					strcpy(fmtPtr, "%R");
					fmtPtr += 2;
					break;
				case 's':
					timezones[1].tzType = tzSidereal;
					strcpy(fmtPtr, "%T");
					fmtPtr += 2;
					break;
			}
		} else
			*fmtPtr++ = *stylePtr++;
	}
	*fmtPtr = 0;
}

static void timezone_layer_update( Layer * const layer, GContext *ctx)
{
	timezone_t *tz = &timezones[containerOf(layer)];
	time_t localUnixTime = time(NULL);
  	time_t layerTime = localUnixTime + tz->tzoffset;
  	struct tm* now = localtime(&layerTime);

	char timebuf[32];
	strftime( timebuf, sizeof(timebuf), timeFmt, now);
	if (usePeriod && now->tm_hour > 11)
		strcat(timebuf, ".");

	const int night_time = (now->tm_hour > 18 || now->tm_hour < 6);

	GRect rect = layer_get_bounds(tz->layer);
	const int w = rect.size.w;
	const int h = rect.size.h;
	int v = time_font > 3 ? 30 : 40;
	int v1 = tz->style[1][0] == 0 ? 10 : 0;
	
	// it is night there, draw in black video
	graphics_context_set_fill_color(ctx, night_time ? GColorBlack : GColorWhite);
	graphics_context_set_text_color(ctx, !night_time ? GColorBlack : GColorWhite);
	graphics_fill_rect(ctx, GRect(0, 0, w, h), 0, 0);
	
	char styleBuf[32];
	if (tz->tzType == tzSidereal)
		strcpy(styleBuf, "Sidereal");
	else
		strftime( styleBuf, sizeof(styleBuf), tz->style[0], now);
	graphics_draw_text(ctx,
					   styleBuf,
					   font[tz->style_font[0]],
					   GRect(0, v1, w, h/2-v1),
					   GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL );
	if (tz->style[1][0] != 0 && tz->tzType != tzSidereal) {
		strftime( styleBuf, sizeof(styleBuf), tz->style[1], now);
		graphics_draw_text(ctx,
					   styleBuf,
					   font[tz->style_font[1]],
					   GRect(0, 20, w, h/2),
					   GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL );
	}
	graphics_draw_text(ctx,
					   timebuf,
					   font[time_font],
					   GRect(0, v, w, h-v),
					   GTextOverflowModeTrailingEllipsis, GTextAlignmentCenter, NULL );
}


/** Called once per minute */
static void handle_tick(struct tm* tick_time, TimeUnits units_changed)
{
	if ((units_changed & MINUTE_UNIT) != 0) {
		calcSiderealOffset();
  		calcStyles();
  		calcTimeFmt();
	}
	for (int i = 0 ; i < NUM_TIMEZONES ; i++)
		layer_mark_dirty(timezones[i].layer);
}
//*********************************
static void in_received_handler(DictionaryIterator *iter, void *context) {
	Tuple *tuple = dict_read_first(iter);
	while (tuple != NULL) {
		if (tuple->type == TUPLE_CSTRING)
			APP_LOG(APP_LOG_LEVEL_DEBUG,"in_received_handler string key %ld = %s", tuple->key, tuple->value->cstring);
		else if (tuple->type == TUPLE_INT)
			APP_LOG(APP_LOG_LEVEL_DEBUG,"in_received_handler int key %ld = %ld", tuple->key, tuple->value->int32);
		else
			APP_LOG(APP_LOG_LEVEL_DEBUG,"in_received_handler key %ld type %d", tuple->key, tuple->type);
		tuple = dict_read_next(iter);
	}
	Tuple *int_tuple = dict_find(iter, AKEY_TZOFFSET);
	if (int_tuple) {
		UTCOffset = int_tuple->value->int32 *60;
		persist_write_int(AKEY_TZOFFSET, UTCOffset);
		timezones[0].selected = UTCOffset;
		persist_write_int(AKEY_LOCALOFFSET, timezones[0].selected);
		if (timezones[1].tzType == tzUTC) {
			timezones[1].tzoffset = UTCOffset;
		} else if (timezones[1].tzType == tzSelected) {
			timezones[1].tzoffset = UTCOffset - timezones[1].selected;
		}
	}
	time_t selected = 0;
	int_tuple = dict_find(iter, AKEY_TZSECONDS);
	if (int_tuple) {
		selected = int_tuple->value->int32;
	} else {
		int_tuple = dict_find(iter, AKEY_TIMEZONE);
		if (int_tuple) {
			char *s = int_tuple->value->cstring;
			bool negative =  (*s == '-');
			if (negative)
				s++;
			while (*s) {
				selected = selected * 10 + *s - '0';
				s++;
			}
			selected *= 60;
			if (negative)
				selected = -selected;
		}
	}
	if (int_tuple) {
		APP_LOG(APP_LOG_LEVEL_DEBUG,"selected offset %ld", selected);
		if (selected == FIFTEEN_MINUTES || selected == -FIFTEEN_MINUTES) {
			timezones[1].tzType = tzSidereal;
		} else {
			timezones[1].tzType = tzSelected;
			timezones[1].tzoffset = UTCOffset - selected;
		}
		persist_write_int(AKEY_TZTYPE, timezones[1].tzType);
		persist_write_int(AKEY_TIMEZONE, selected);
		time_t reselected = persist_read_int(AKEY_TIMEZONE);
		APP_LOG(APP_LOG_LEVEL_DEBUG,"selected offset reads back as %ld", reselected);
		timezones[1].selected = selected;
	}
	int_tuple = dict_find(iter, AKEY_TIMESTYLE);
	if (int_tuple) {
		strcpy(timeStyle, int_tuple->value->cstring);
		persist_write_string(AKEY_TIMESTYLE, timeStyle);
		tick_timer_service_unsubscribe();
		tick_timer_service_subscribe(*timeStyle == 's' ? SECOND_UNIT : MINUTE_UNIT, &handle_tick);
	}
	int_tuple = dict_find(iter, AKEY_CITY);
	if (int_tuple) {
		strcpy(myCity, int_tuple->value->cstring);
		persist_write_string(AKEY_CITY, myCity);
		APP_LOG(APP_LOG_LEVEL_DEBUG, "my city = %s", myCity);
	}
	int_tuple = dict_find(iter, AKEY_TZCITY);
	if (int_tuple) {
		strcpy(tzCity, int_tuple->value->cstring);
		persist_write_string(AKEY_TZCITY, tzCity);
		APP_LOG(APP_LOG_LEVEL_DEBUG, "TZ city = %s", tzCity);
	}
	int_tuple = dict_find(iter, AKEY_LONGITUDE);
	if (int_tuple) {
		if (our_longitude_int != int_tuple->value->int32) {
			our_longitude_int = int_tuple->value->int32;
			persist_write_int(AKEY_LONGITUDE, our_longitude_int);
			our_longitude = our_longitude_int;
			our_longitude /= 1000000.0;
		}
	}
	int_tuple = dict_find(iter, AKEY_STYLE);
	if (int_tuple) {
		strcpy(our_style, int_tuple->value->cstring);
		persist_write_string(AKEY_STYLE, our_style);
	}
	time_t now = time(NULL);
	struct tm *current_time = localtime(&now);
	handle_tick(current_time, MINUTE_UNIT);
}

#define ConstantGRect(x, y, w, h) {{(x), (y)}, {(w), (h)}}

void init()
{
	const bool animated = true;
	window = window_create();
	window_stack_push(window, animated);
	window_set_background_color(window, GColorBlack);

	font[0] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_GOTHAMXNARROW_BOLD_15));
	font[1] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_BOLD_20));
	font[2] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_BOLD_24));
	font[3] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_BOLD_30));
	font[4] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_BOLD_SUBSET_45));
	font[5] = fonts_load_custom_font(resource_get_handle(RESOURCE_ID_FONT_ROBOTO_BOLD_SUBSET_49));

	for (int i = 0 ; i < NUM_TIMEZONES ; i++)
	{
		timezone_t * const tz = &timezones[i];
		GRect rect = ConstantGRect(0, i * LAYER_HEIGHT, PEBBLE_SCREEN_WIDTH, LAYER_HEIGHT);
		tz->layer = layer_create(rect);
		layer_set_update_proc(tz->layer, timezone_layer_update);
  		layer_add_child(window_get_root_layer(window), tz->layer);
		layer_mark_dirty(tz->layer);
	}
	// Read last values if bluetooth is off or can't communicate with phone
	strcpy(myCity, "waiting...");
	persist_read_string(AKEY_CITY, myCity, sizeof(myCity));
	strcpy(tzCity, "waiting...");
	persist_read_string(AKEY_TZCITY, tzCity, sizeof(tzCity));
	strcpy(timeStyle, "mpFpR");
	APP_LOG(APP_LOG_LEVEL_DEBUG, "new TZ city = %s", tzCity);
	persist_read_string(AKEY_TIMESTYLE, timeStyle, sizeof(timeStyle));
	UTCOffset = persist_read_int(AKEY_TZOFFSET);
	timezones[0].selected = persist_read_int(AKEY_LOCALOFFSET);
	time_t selected = persist_read_int(AKEY_TIMEZONE);
	timezones[1].selected = selected;
	APP_LOG(APP_LOG_LEVEL_DEBUG,"selected offset %ld", selected);
	timezones[1].tzType = persist_read_int(AKEY_TZTYPE);
	if (timezones[1].tzType == tzUTC)
		timezones[1].tzoffset = UTCOffset;
	else
		timezones[1].tzoffset = UTCOffset - selected;
	
	our_longitude_int = persist_read_int(AKEY_LONGITUDE);
	our_longitude = our_longitude_int;
	our_longitude /= 1000000.0;
	
	strcpy(our_style, "p$1|C$g|1|C$g $a");
	persist_read_string(AKEY_STYLE, our_style, sizeof(our_style));

	tick_timer_service_subscribe(*timeStyle == 's' ? SECOND_UNIT : MINUTE_UNIT, &handle_tick);
	app_message_register_inbox_received(in_received_handler);
	const uint32_t inbound_size = 256;
	const uint32_t outbound_size = 128;
	app_message_open(inbound_size, outbound_size);
	time_t now = time(NULL);
	struct tm *current_time = localtime(&now);
	handle_tick(current_time, MINUTE_UNIT);
}

void deinit()
{
	app_message_deregister_callbacks();
	tick_timer_service_unsubscribe();
	for (int i = 0 ; i < NUM_TIMEZONES ; i++)
	{
		timezone_t * const tz = &timezones[i];
		layer_destroy(tz->layer);
	}
	for (int i = 0 ; i < FONT_COUNT ; i++)
		fonts_unload_custom_font(font[i]);
	window_destroy(window);
}
#if false
int main(void)
{
	init();
	app_event_loop();
  	deinit();
}
#endif